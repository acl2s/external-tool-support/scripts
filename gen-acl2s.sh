#!/usr/bin/env bash

# This script will create an executable file, acl2s, by building ACL2s.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment variable ACL2S_SCRIPTS to the top-level
# directory of this repository.
#
# Next, set the environment ACL2S_NUM_JOBS to be the number of jobs
# you want to run in parallel. This allows us to build books in
# parallel. A good rule of thumb is set this to the number of cores on
# your machine.

if [[ -z "${ACL2_SYSTEM_BOOKS}" ]]; then
    echo "Error: the environement variable ACL2_SYSTEM_BOOKS must be defined!"
    echo "Set ACL2_SYSTEM_BOOKS as per the cert.pl instructions"
    exit 1
fi

if [[ -z "${ACL2S_SCRIPTS}" ]]; then
    echo "Error: the environement variable ACL2S_SCRIPTS must be defined!"
    echo "Set ACL2S_SCRIPTS to the top-level directory of this repository."
    exit 1
fi

if [[ -z "${ACL2S_NUM_JOBS}" ]]; then
    echo "Error: the environement variable ACL2S_NUM_JOBS must be defined!"
    echo "Set ACL2S_NUM_JOBS to be the number of jobs you want to run in parallel."
    echo "A good rule of thumb is to set this to the number of cores on your machine."
    exit 1
fi

ACL2_DIR_FROM_BOOKS=`dirname "${ACL2_SYSTEM_BOOKS}" `

if ! command -v acl2 &> /dev/null
then
    echo "The acl2 executable is not in your path!"
    echo "Create a symbolic link named \"acl2\" that is in your path"
    echo "and points to the ACL2 executable."
    echo "In a directory in your path type"
    echo "ln -s ${ACL2_DIR_FROM_BOOKS}/saved_acl2 acl2" 
    exit 1
fi


cd "$ACL2_SYSTEM_BOOKS"/acl2s

if "$ACL2_SYSTEM_BOOKS"/build/cert.pl -j "$ACL2S_NUM_JOBS" top.lisp; then
    # Versions of ACL2 after 96d3801ff48d07e5fccf468d06010f98426534f3
    # will require an additional file to be certified.
    # This file was previously a dependency of acl2s/top, so explicitly
    # certifying it will just be a no-op on versions of ACL2 before the
    # problematic commit.
    if "$ACL2_SYSTEM_BOOKS"/build/cert.pl -j "$ACL2S_NUM_JOBS" mode-acl2s-dependencies.lisp; then
        : # noop
    else
        echo "Certification failed"
        exit 1
    fi
    cd -
    acl2 < "$ACL2S_SCRIPTS"/gen-acl2s.lisp
    exit 0
else
    echo "Certification failed"
    exit 1
fi
