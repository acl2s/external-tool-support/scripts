#!/usr/bin/env bash

# This script will rebuild SBCL.
# 
# Set the environment variable SBCL_DIR to the SBCL directory

if [[ -z "${SBCL_DIR}" ]]; then
    echo "Error: The environement variable SBCL_DIR must be defined!"
    echo "Make sure SBCL_DIR points to the SBCL directory."
    exit 1
fi

# Provide either -n or --no-git to prevent a `git pull` from being performed.
GIT=1
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -n|--no-git) GIT=0; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

cd $SBCL_DIR
if [[ GIT -gt 0 ]]; then
    git pull
fi
sh make.sh --without-immobile-space --without-immobile-code --without-compact-instance-header --fancy --dynamic-space-size=32Gb

# Make sure that the account that is running this script has
# permissions to modify the files in the installation directory.
# You could also run the following command using "sudo".
sh install.sh
