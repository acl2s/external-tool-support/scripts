(ld "acl2s/package.lsp" :dir :system)
(in-package "ACL2S")

(include-book "acl2s/top" :dir :system)
(include-book "xdoc/save" :dir :system) 

(defxdoc acl2::top       
  :short "ACL2s Manual"
  :long "<p>This is the ACL2s Manual</p>")

;; Turning off guard checking since xdoc::save generates errors
;; otherwise.

(set-guard-checking nil)
(xdoc::save "./acl2s-manual" :redef-okp t :error t)
