#!/usr/bin/env bash

# This script will create an executable file, acl2s, by building ACL2s.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment variable ACL2S_SCRIPTS to the top-level
# directory of this repository.

if [[ -z "${ACL2_SYSTEM_BOOKS}" ]]; then
    echo "Error: the environement variable ACL2_SYSTEM_BOOKS must be defined!"
    echo "Set ACL2_SYSTEM_BOOKS as per the cert.pl instructions"
    exit 1
fi

if [[ -z "${ACL2S_SCRIPTS}" ]]; then
    echo "Error: the environement variable ACL2S_SCRIPTS must be defined!"
    echo "Set ACL2S_SCRIPTS to the top-level directory of this repository."
    exit 1
fi

cd "$ACL2_SYSTEM_BOOKS"/acl2s
if cert.pl -j 4 top.lisp; then 
    cd -
    "$ACL2" < "$ACL2S_SCRIPTS"/../gen-acl2s.lisp
else
    echo "Certification failed"
fi
