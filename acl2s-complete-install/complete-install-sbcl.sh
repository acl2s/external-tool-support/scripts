#!/usr/bin/env bash

# This script will build SBCL.
# 
# Set the environment variable SBCL_DIR to the SBCL directory

if [[ -z "${SBCL_DIR}" ]]; then
    echo "Error: The environement variable SBCL_DIR must be defined!"
    echo "Make sure SBCL_DIR points to the SBCL directory."
    exit 1
fi

cd "$SBCL_DIR"
sh make.sh --prefix="$SBCL_INSTALL" --without-immobile-space --without-immobile-code --without-compact-instance-header --fancy --dynamic-space-size=4Gb
unset SBCL_HOME
INSTALL_ROOT="$SBCL_INSTALL" sh install.sh
