#!/usr/bin/env bash

# This script will create a new ACL2 executable and clean all books
# and then it will create an executable file, acl2s, by building ACL2s.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment variable ACL2S_SCRIPTS to the top-level
# directory of this repository.
#
# Next, set the environment variable ACL2_LISP to the lisp you use to build
# ACL2 (SBCL is what I use)

if [[ -z "${ACL2S_SCRIPTS}" ]]; then
    echo "Error: the environment variable ACL2S_SCRIPTS must be defined!"
    echo "Set ACL2S_SCRIPTS to the top-level directory of this repository."
    exit 1
fi

# Rebuild ACL2 and clean existing books
$ACL2S_SCRIPTS/clean-gen-acl2.sh $@

# Rebuild ACL2s
$ACL2S_SCRIPTS/gen-acl2s.sh $@
