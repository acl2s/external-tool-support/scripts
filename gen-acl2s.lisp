(make-event `(add-include-book-dir :acl2s-modes (:system . "acl2s/")))
(ld "acl2s-mode.lsp" :dir :acl2s-modes)
(acl2s-defaults :set verbosity-level 1)
(set-inhibit-warnings! "Invariant-risk" "theory")
(time-tracker nil) ; turn off tau time messages

;; Make guard violations more readable
(set-print-gv-defaults :conjunct t :substitute t)

;; Prevent theory events from stacking up if a book is LDed many times:
(set-in-theory-redundant-okp t)

;; Show more info when :monitoring rules:
(set-brr-evisc-tuple nil state)

;; Make everything print as lower case:
(set-print-case :downcase state)

(reset-prehistory t)
(in-package "ACL2S")

:q

(in-package "ACL2")

; Defined in acl2-init.lisp and is set to 32000, but in one of the
; projects, we ran out of space and while that was due to make-event
; issues that should be dealt with, I don't see a downside in
; increasing this number.
#+sbcl
(setf *sbcl-dynamic-space-size* 128000)

(defun print-ttag-note (val active-book-name include-bookp deferred-p state)
  (declare (xargs :stobjs state)
           (ignore val active-book-name include-bookp deferred-p))
  state)

(defun print-deferred-ttag-notes-summary (state)
  (declare (xargs :stobjs state))
  state)

(defun notify-on-defttag (val active-book-name include-bookp state)
  (declare (xargs :stobjs state)
           (ignore val active-book-name include-bookp))
    state)

; Modified from basis-a.lisp
(defun save-exec-fn (exec-filename extra-startup-string host-lisp-args
                                   toplevel-args inert-args return-from-lp
                                   init-forms)

  #-acl2-loop-only
  (progn

    (when (not (our-probe-file (directory-namestring exec-filename)))

; Without this check, CCL will create a directory for us; yet SBCL will not.
; We prefer consistent behavior across all Lisps.  Here we choose to require
; the directory to exist already, to prevent users from creating directories
; they don't want by mistake.

      (error "~s is unable to save to file ~s, because its directory does not ~
              exist."
             'save-exec exec-filename))

; Parallelism blemish: it may be a good idea to reset the parallelism variables
; in all #+acl2-par compilations before saving the image.

    (when (and init-forms return-from-lp)

; For each of return-from-lp and init-forms, a non-nil value takes us through a
; different branch of LP.  Rather than support the use of both, we cause an
; error.

      (er hard 'save-exec
          "The use of non-nil values for both :init-forms and :return-from-lp ~
           is not supported for save-exec.  Consider using only :init-forms, ~
           with (value :q) as the final form."))
    (setq *return-from-lp* return-from-lp)
    (setq *lp-init-forms* init-forms)
    #-sbcl (when toplevel-args
             (er hard 'save-exec
                 "Keyword argument :toplevel-args is only allowed when the ~
                  host Lisp is SBCL."))
    (if (not (eql *ld-level* 0))
        (er hard 'save-exec
            "Please type :q to exit the ACL2 read-eval-print loop and then try ~
             again."))
    (if (equal extra-startup-string "")
        (er hard 'save-exec
            "The extra-startup-string argument of save-exec must be ~x0 or ~
             else a non-empty string."
            nil)
      (setq *saved-string*
            (format
             nil
             "~%~a~%"
             (cond ((null extra-startup-string)
                    "This ACL2 executable was created by saving a session.")
                   (t extra-startup-string)))))
    #-(or gcl cmu sbcl allegro clisp ccl lispworks)
    (er hard 'save-exec
        "Sorry, but save-exec is not implemented for this Common Lisp.")

; The forms just below, before the call of save-exec-raw, are there so that the
; initial (lp) will set the :cbd correctly.

    (f-put-global 'connected-book-directory nil *the-live-state*)
    (setq *initial-cbd* nil)
    (setq *startup-package-name* (package-name *package*))
    (setq *saved-build-date-lst*

; By using setq here for *saved-build-date* instead of a let-binding for
; save-exec-raw, it happens that saving more than once in the same session (for
; Lisps that allow this, such as Allegro CL but not GCL) would result in extra
; "; then ..." strings.  But that seems a minor problem, and avoids having to
; think about the effect of having a let-binding in force above a save of an
; image.

          (cons (saved-build-date-string)
                *saved-build-date-lst*))
    (save-exec-raw exec-filename
                   host-lisp-args
                   #+sbcl toplevel-args
                   inert-args))
  #+acl2-loop-only
  (declare (ignore exec-filename extra-startup-string host-lisp-args
                   toplevel-args inert-args return-from-lp init-forms))
  nil ; Won't get to here in GCL and perhaps other lisps
  )

; Modified from acl2-init.lisp. Added --noinform option
#+sbcl
(defun save-acl2-in-sbcl-aux (sysout-name core-name
                                          host-lisp-args
                                          toplevel-args
                                          inert-args)

; Note that host-lisp-args specifies what the SBCL manual calls "runtime
; options", while toplevel-args is what it calls "toplevel options".

  (declare (optimize (sb-ext:inhibit-warnings 3)))
  (let* ((use-thisscriptdir-p (use-thisscriptdir-p sysout-name core-name))
         (eventual-sysout-core
          (unix-full-pathname core-name "core"))
         (sysout-core
          (unix-full-pathname sysout-name "core")))
    (if (probe-file sysout-name)
        (delete-file sysout-name))
    (if (probe-file eventual-sysout-core)
        (delete-file eventual-sysout-core))
    (when use-thisscriptdir-p
      (setq eventual-sysout-core
            (concatenate 'string "$THISSCRIPTDIR/" core-name ".core")))
    (with-open-file ; write to nsaved_acl2
      (str sysout-name :direction :output)
      (let* ((prog (car sb-ext:*posix-argv*)))
        (write-exec-file
         str
         ("~a~a~%"
          (if use-thisscriptdir-p *thisscriptdir-def* "")
          (format nil
                  "export SBCL_HOME='~a'"
                  *sbcl-home-dir*))

; We have observed with SBCL 1.0.49 that "make HTML" fails on our 64-bit linux
; system unless we start sbcl with --control-stack-size 4 [or larger].  The
; default, according to http://www.sbcl.org/manual/, is 2.  The problem seems
; to be stack overflow from fmt0, which is not tail recursive.  More recently,
; community book centaur/misc/defapply.lisp causes a stack overflow even with
; --control-stack-size 4 (though that might disappear after we added (comp t)
; in a couple of places).  Yet more recently, community books
; books/centaur/regression/common.lisp and books/centaur/tutorial/intro.lisp
; fail with --control-stack-size 8, due to calls of def-gl-clause-processor.
; So we use --control-stack-size 16.  We increased 16 to 64 on 10/22/2015 at
; the request of Jared Davis, in support of a Verilog parser.

; See *sbcl-dynamic-space-size* for an explanation of the --dynamic-space-size
; setting below.

; Note that --no-userinit was introduced into SBCL in Version 0.9.13, hence has
; been part of SBCL since 2007 (perhaps earlier).  So when Jared Davis pointed
; out this option to us after ACL2 Version_6.2, we started using it in place of
; " --userinit /dev/null", which had not worked on Windows.

; In July 2017 we added ${SBCL_USER_ARGS} below to accommodate Sol Swords's
; request to be able to pass runtime-options without having to call save-exec.
; Example:
; (export SBCL_USER_ARGS="--lose-on-corruption" ; ./sbcl-saved_acl2)

; On October 9, 2020 we added --tls-limit 8192, because certification of
; community book books/kestrel/apt/schemalg-template-proofs.lisp failed with
; ACL2 built on SBCL.  The error was "Thread local storage exhausted", which we
; apparently indicates too many special variables.  The SBCL 1.5.2 release
; notes say that "command-line option "--tls-limit" can be used to alter the
; maximum number of thread-local symbols from its default of 4096".  We chose
; 8192 because it was sufficient for the book above, but perhaps it can be
; increased significantly more without bad effect (not sure).  But then in
; March 2021, on a Mac, that same book again failed with the same error; so we
; doubled the tls-limit.

         "~s --tls-limit 16384 --dynamic-space-size ~s --control-stack-size ~
          64 --disable-ldb --noinform --core ~s~a ${SBCL_USER_ARGS} ~
          --end-runtime-options --no-userinit --eval '(acl2::sbcl-restart)'~a ~a~%"
         prog
         *sbcl-dynamic-space-size*
         eventual-sysout-core
         (insert-string host-lisp-args)
         (insert-string toplevel-args)
         (user-args-string inert-args "--end-toplevel-options"))))
    (chmod-executable sysout-name)

; In SBCL 0.9.3 the read-only space is too small for dumping ACL2 on x86, so we
; have to specify :PURIFY NIL. This will unfortunately result in some core file
; bloat, and slightly slower startup.

    (sb-ext:gc)
    (sb-ext:save-lisp-and-die sysout-core
                              :purify
                              #+(or x86 x86-64 ppc) nil
                              #-(or x86 x86-64 ppc) t)))


(in-package "ACL2S")

(save-exec
 "acl2s"
 (format
  nil
  "~a.~
   ~%Build date: ~a.~
   ~%Copyright (C) 2022, Northeastern University.~
   ~%ACL2s is an extension of ~a.~
   ~%ACL2s comes with ABSOLUTELY NO WARRANTY.~
   ~%This is free software with certain restrictions.~
   ~%See the LICENSE files distributed with ACL2s and ACL2."
  acl2s::*acl2s-version*
  (acl2::saved-build-date-string)
  acl2::*copy-of-acl2-version*))
