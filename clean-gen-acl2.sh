#!/usr/bin/env bash

# This script will create a new ACL2 executable and clean all books.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment variable ACL2S_SCRIPTS to the top-level
# directory of this repository.
#
# Next, set the environment variable ACL2_LISP to the lisp you use to build
# ACL2 (SBCL is what I use)
#
# Next, set the environment ACL2S_NUM_JOBS to be the number of jobs
# you want to run in parallel. This allows us to build books in
# parallel. A good rule of thumb is set this to the number of cores on
# your machine.

if [[ -z "${ACL2_SYSTEM_BOOKS}" ]]; then
    echo "Error: the environment variable ACL2_SYSTEM_BOOKS must be defined!"
    echo "Set ACL2_SYSTEM_BOOKS as per the cert.pl instructions"
    exit 1
fi

if [[ -z "${ACL2_LISP}" ]]; then
    echo "Error: the environment variable ACL2_LISP must be defined!"
    echo "Set ACL2_LISP to the lisp you use to build ACL2 (SBCL is a good choice)."
    exit 1
fi

if [[ -z "${ACL2S_NUM_JOBS}" ]]; then
    echo "Error: the environment variable ACL2S_NUM_JOBS must be defined!"
    echo "Set ACL2S_NUM_JOBS to be the number of jobs you want to run in parallel."
    echo "A good rule of thumb is to set this to the number of cores on your machine."
    exit 1
fi

# Provide either -n or --no-git to prevent a `git pull` from being performed.
# Provide either -a or --all to perform a full ACL2 distribution build.
GIT=1
ALL=0
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -n|--no-git) GIT=0; shift ;;
        -a|--all) ALL=1; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# Clean existing books
cd $ACL2_SYSTEM_BOOKS/../
if [[ GIT -gt 0 ]]; then
    git pull
fi
make -j "$ACL2S_NUM_JOBS" clean-books &
make -j "$ACL2S_NUM_JOBS" clean-all

# Rebuild ACL2

make -j "$ACL2S_NUM_JOBS" LISP=$ACL2_LISP

# This became necessary sometime around June 23rd 2023
# Without this, cert.pl will error out when trying to certify
# certain books that require some of the certdeps in
# ACL2_SYSTEM_BOOKS/build
cd $ACL2_SYSTEM_BOOKS
make build/Makefile-features
cd -

if [[ ALL -gt 0 ]]; then
    make -j "$ACL2S_NUM_JOBS" ACL2=$ACL2_SYSTEM_BOOKS/../saved_acl2 regression
fi

